﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    public GameObject menu1;
    public GameObject menu2;


    public void OpenMenu2()
    {
        menu1.active = false;
        menu2.active = true;
    }

    public void OpenMenu1()
    {
        menu1.active = true;
        menu2.active = false;
    }

    public void QuitGame()
    { 
        Application.Quit();
    }

    public void ChangeLevel(string levelName)
    {
        SceneManager.LoadScene(levelName);
    }
}
