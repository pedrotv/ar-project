﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MovePlayer : MonoBehaviour
{
    //public GameObject mainCamera;
    public Joystick joystick;
    public GameObject playerModel;
    public GameObject trailParticles;
    public GameObject[] hearts;
    public float jumpSpeed = 12.0f;
    public float gravity = 10.0f;
    public bool isGrounded = false;
    
    private float invencibilityTimer;
    private float health = 3;
    private float speed = 12.0f;                                           
    private bool isInvencible = false;
    private Vector3 iniPos;

    //private Vector3 initCamPos;
    private Vector3 moveDirection = Vector3.zero;
    //private CharacterController controller;
    private Animator animator;
    
    void Start()
    {
        iniPos = transform.position;
       //controller = GetComponent<CharacterController>();
       // initCamPos = mainCamera.transform.position;
        animator = playerModel.GetComponent<Animator>();
        trailParticles.SetActive(false);
    }

    void Update()
    {
        //Movement Arrows
        //moveDirection.x = Input.GetAxis("Horizontal") * speed;
        //moveDirection.z = Input.GetAxis("Vertical") * speed;
        //moveDirection = transform.TransformDirection(moveDirection);

        //Movement joystick
        moveDirection.x = joystick.Horizontal * speed;
        moveDirection.z = joystick.Vertical * speed;
        //moveDirection = transform.TransformDirection(moveDirection);

        if (joystick.Horizontal == 0 && joystick.Vertical == 0 && isGrounded)
        {
            moveDirection = new Vector3(0, 0, 0);
        }

        //Jump
        if (Input.GetKeyDown("space"))
        {
            OnclickJumpButton();
        }

        // Apply gravity
        if (!isGrounded)
            moveDirection.y = moveDirection.y - (gravity * Time.deltaTime);
        else
            moveDirection.y = 0;

        // Move the controller
        transform.Translate(moveDirection * Time.deltaTime);
        //controller.Move(moveDirection * Time.deltaTime);

        //Run Animation
        Vector3 actualSpeed = moveDirection;
        float vel = actualSpeed.x * actualSpeed.x + actualSpeed.z * actualSpeed.z;
        if (isGrounded)
        {
            animator.SetFloat("Speed", vel);

            //Run particles
            if (vel > 1.5f)
            {
                trailParticles.SetActive(true);
            }
            else
            {
                trailParticles.SetActive(false);
            }
        }
        else
        {
            trailParticles.SetActive(false);
        }

        //Rotation
        if (vel > 0.1)
            playerModel.transform.localEulerAngles = new Vector3(0, (Mathf.Atan2(joystick.Vertical, joystick.Horizontal) * -180/ Mathf.PI)+90, 0);
            
        //Move camera
        //mainCamera.transform.position = new Vector3(transform.position.x, initCamPos.y, transform.position.z - 15);

        //Fall
        if (transform.position.y <= -50)
        {
            transform.position = iniPos + new Vector3(0, 3, 0);
        }

        //Invencibility
        if (isInvencible)
        {
            invencibilityTimer += 1 * Time.deltaTime;
            //Debug.Log(invencibilityTimer);
            if (invencibilityTimer >= 3)
            {
                isInvencible = false;
            }
        }

        //Respawn
        if (health <= 0)
        {
            transform.position = iniPos + new Vector3(0,3,0);
            health = 3;
            for (int i = 0; i < 3; i++)
                hearts[i].active = true;
        }

    }

    public void OnclickJumpButton()
    {
        if (isGrounded)
        {
            animator.SetFloat("Speed", 0.03f);
            isGrounded = false;
            animator.SetTrigger("Jump");
            moveDirection.y = jumpSpeed;
        }
    }

    void OnCollisionEnter(Collision hit)
    {
        Debug.Log("Enter: " + hit.gameObject.name);

        //Take dmg
        if (hit.gameObject.tag == "Enemy")
        {
            if (!isInvencible)
            {
                health -= 1;
                isInvencible = true;
                invencibilityTimer = 0;

                //Adjust health
                if (health == 3)
                {
                    for (int i = 0; i < 3; i++)
                        hearts[i].active = true;
                }
                else if (health == 2)
                {
                    hearts[2].active = false;
                    hearts[1].active = true;
                    hearts[0].active = true;
                }
                else if (health == 1)
                {
                    hearts[2].active = false;
                    hearts[1].active = false;
                    hearts[0].active = true;
                }
                else if (health == 0)
                {
                    for (int i = 0; i < 3; i++)
                        hearts[i].active = false;
                }
            }
        }

        //Jumped on enemy head
        if (hit.gameObject.tag == "EnemyHead")
        {
            Destroy(hit.gameObject);
            animator.SetFloat("Speed", 0.03f);
            animator.SetTrigger("Jump");
            moveDirection.y = jumpSpeed/1.25f;
        }

        if (hit.gameObject.tag == "Ground")
        {
            animator.SetTrigger("Floor");
        }
    }

    private void OnCollisionStay(Collision hit)
    {
        //Debug.Log(hit.gameObject.name);
        //If is on the ground
        if (hit.gameObject.tag == "Ground")
        {
            isGrounded = true;
        }
        else
        {
            isGrounded = false;
        }
    }

}