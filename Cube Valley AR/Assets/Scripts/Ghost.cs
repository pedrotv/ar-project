﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ghost : Enemies
{
    public GameObject headCollision;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    protected override void Update()
    {
        float distance = Vector3.Distance(paths[nPath].transform.position, transform.position);
        if (distance > maxDist)
        {
            transform.position = Vector3.MoveTowards(transform.position, paths[nPath].transform.position, movementSpeed);
            transform.position = new Vector3(transform.position.x, 0, transform.position.z);
        }

        //base.lookToTarget();

        if (headCollision == null)
        {
            Destroy(gameObject);
        }

        transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0);
    }

}
