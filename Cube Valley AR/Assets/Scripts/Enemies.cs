﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemies : MonoBehaviour
{
    enum BehaviourState { walking, follow, attacking};
    BehaviourState behaviourState;

    public int hitPoints = 1;
    public float movementSpeed = 0.1f;
    public float rotationSpeed = 0.4f;
    public float relativeRotation = 0.0f;
    public float maxDist = 0.3f;
    public GameObject[] paths;
    public int nPath = 0;
    public bool randomPath = false;
    Quaternion targetRotation;
       
    protected virtual void Start()
    {
        behaviourState = BehaviourState.walking;
    }
    

    protected virtual void Update()
    {
        BehaviourManager();
        transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0);
    }

    void BehaviourManager()
    {
        switch (behaviourState)
        {
            case BehaviourState.walking:
                WalkingBehaviour();
                break;
            case BehaviourState.follow:
                break;
            case BehaviourState.attacking:
                break;
            default:
                break;
        }
    }

    void WalkingBehaviour()
    {
        float distance = Vector3.Distance(paths[nPath].transform.position, transform.position);
        transform.position = Vector3.MoveTowards(transform.position, paths[nPath].transform.position, movementSpeed);
        lookToTarget();

        if (!randomPath)
        {
            if (distance < maxDist)
            {
                nPath++;
            }
            if(nPath > paths.Length - 1)
            {
                nPath = 0;
            }
        }
        else
        {
            if (distance < maxDist)
            {
                nPath = Random.Range(0, paths.Length - 1);
            }
        }

    }

    protected void lookToTarget()
    {
        Vector3 relativePosition = paths[nPath].transform.position - transform.position;
        targetRotation = Quaternion.LookRotation(relativePosition);
        relativeRotation = Time.deltaTime * rotationSpeed;
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, relativeRotation);
    }
}
