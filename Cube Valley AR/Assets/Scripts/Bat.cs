﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bat : Enemies
{
    public GameObject headCollision;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    protected override void Update()
    {
        if (headCollision == null)
        {
            Destroy(gameObject);
        }
        base.Update();
    }
}
